#pragma once

#include <array>
#include <unordered_map>
#include <mutex>

#include "event/event.hpp"

#include "input/computer.hpp"

namespace input
{
	
	namespace axis
	{
		namespace joystick
		{
			struct event_data_t
			{
				int axis;
				float value;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
		}
		struct deadzone_descriptor
		{
			float positive_deadzone;
			float negative_deadzone;
		};
		extern std::unordered_map<int, deadzone_descriptor> deadzones;
		extern std::mutex deadzones_mutex;
		namespace joystick_button
		{
			struct event_data_t
			{
				uint8_t button;
				bool is_down;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
		}
	}
}
