#pragma once
#include "input/computer.hpp"
#include "input/axis.hpp"

namespace input
{
	struct interfaces
	{
		computer::scan_code::interface_t scan_code = {computer::scan_code::manager, "interfaces_if"};
		computer::text_input::interface_t text_input = {computer::text_input::manager, "interfaces_if"};
		computer::mouse_motion::interface_t mouse_motion = {computer::mouse_motion::manager, "interfaces_if"};
		computer::mouse_button::interface_t mouse_button = {computer::mouse_button::manager, "interfaces_if"};
		computer::clipboard::interface_t clipboard = {computer::clipboard::manager, "interfaces_if"};
		computer::quit::interface_t quit = {computer::quit::manager, "interfaces_if"};

		axis::joystick::interface_t joystick = {axis::joystick::manager, "interfaces_if"};
		axis::joystick_button::interface_t joystick_button = {axis::joystick_button::manager, "interfaces_if"};

		inline void dispatch_events()
		{
			scan_code.dispatch_events();
			text_input.dispatch_events();
			mouse_motion.dispatch_events();
			mouse_button.dispatch_events();
			clipboard.dispatch_events();
			quit.dispatch_events();

			joystick.dispatch_events();
			joystick_button.dispatch_events();
		}
	};
};
