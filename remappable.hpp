#pragma once

#include "interfaces.hpp"

namespace input
{
	namespace generic_axis
	{
		using event_data_t = float;
		using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
		using manager_t = event_configuration_types::manager_t;
		using interface_t = event::endpoint_interface<event_configuration_types>;
	}
	namespace generic_button
	{
		using event_data_t = bool;
		using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
		using manager_t = event_configuration_types::manager_t;
		using interface_t = event::endpoint_interface<event_configuration_types>;
	}

	template<typename T>
	concept has_size_element = requires()
	{	
		T::SIZE;
	};
	template<typename manager_t, has_size_element types_enum>
	struct event_module_based_managers_t
	{
		std::array<manager_t, static_cast<size_t>(types_enum::SIZE)> managers;

		void dump_event(types_enum type, auto data)
		{
			using vector_t = typename manager_t::queued_events_t;
			auto data_vec = vector_t{data};
			managers[static_cast<size_t>(type)].dump_events(data_vec,nullptr);
		}
		manager_t& operator[](types_enum enum_in)
		{
			size_t in = static_cast<size_t>(enum_in);
			assert(in < static_cast<size_t>(types_enum::SIZE));
			return managers[in];
		}

	};
	template<has_size_element types_enum>
	using event_module_based_managers_axis_t = event_module_based_managers_t<generic_axis::manager_t, types_enum>;
	template<has_size_element types_enum>
	using event_module_based_managers_button_t = event_module_based_managers_t<generic_button::manager_t, types_enum>;

	/*template<typename event_data_t, auto _binding>
	struct mapped_value;
	{
		static constexpr binding = _binding;

		event_data_t value;
		val_t get()
		{
			return value;
		}
		val_t operator*()
		{
			return value;
		}
	};

	template<typename event_data_t, has_size_element types_enum>
	struct raw_event_struct_based_managers_t
	{
		std::array<event_data_t, static_cast<size_t>(types_enum::SIZE)> managers;

		void dump_event(types_enum type, event_data_t data)
		{
			managers[static_cast<size_t>(type)] = data;
		}
	};*/


	/*!
	 * Provides a way to bind scancode events and joystick events into
	 * `generic` axis events.
	 *
	 * Simply pass in an enum class whose last member is SIZE, 
	 * and then you can bind a key/joystick to any axis in the enum
	 */
	template<has_size_element axis_types_enum, template<typename> typename managers_creator_t = event_module_based_managers_axis_t>
	struct remappable_axes_t 
	{
		using scancode_t = uint32_t;
		using joystick_axis_t = int;
		using val_t = float;

		using interface_t = generic_axis::interface_t;

		static constexpr scancode_t null_scancode = std::numeric_limits<scancode_t>::max();

		managers_creator_t<axis_types_enum> managers;

		auto& operator[](axis_types_enum enum_in)
		{
			return managers[enum_in];
		}

		inline remappable_axes_t(input::interfaces & input_if, auto ... input) : 
			managers(input...),
			scan_code_listener(input_if.scan_code,[this](auto in)
			{
				auto mapping_it = scancode_mapping.find(in.scan_code);
				if (mapping_it != scancode_mapping.end())
				{
					auto data = generic_axis::event_data_t{in.is_down? mapping_it->second.val_pressed : mapping_it->second.val_unpressed};

					//If there are other keys controlling this axis, AND this was a released event, then don't send anything
					//You can imagine how annoying it'd be if you held "w" for a second too long after pressing "s", and then had your
					//character stop after releasing "w".
					if (num_keys_controlling_axis[static_cast<size_t>(mapping_it->second.axis)] == in.scan_code || in.is_down)
					{
						//This kind of violates the event system API model, but is also sort of neccessary (only other way to push events is through an interface,
						//which has additional overhead)
						managers.dump_event(mapping_it->second.axis, data);
						num_keys_controlling_axis[static_cast<size_t>(mapping_it->second.axis)] = in.scan_code;
					}
				}
			},{}),
			joystick_listener(input_if.joystick,[this](auto in)
			{
				auto mapping_it = joystick_mapping.find(in.axis);
				if (mapping_it != joystick_mapping.end())
				{
					auto data = generic_axis::event_data_t{in.value * mapping_it->second.bias_coeffecient};
					managers.dump_event(mapping_it->second.axis,data);
				}
			},{})
		{
			num_keys_controlling_axis.fill(null_scancode);
		}

		void hook_key_to_axis(scancode_t key, axis_types_enum axis, val_t val_unpressed, val_t val_depressed)
		{
			scancode_mapping.insert(std::make_pair(key, key_descriptor{axis, val_unpressed, val_depressed}));
		}
		bool is_key_bound(scancode_t key)
		{
			return scancode_mapping.find(key) != scancode_mapping.end();
		}
		void unbind_key(scancode_t key)
		{
			scancode_mapping.erase(key);
		}

		void hook_joystick_to_axis(joystick_axis_t joystick_axis, axis_types_enum generic_axis, val_t bias_coeffecient=1.0)
		{
			joystick_mapping.insert(std::make_pair(joystick_axis, joystick_descriptor{generic_axis,bias_coeffecient}));
		}
		bool is_joystick_bound(joystick_axis_t joystick_axis)
		{
			return joystick_mapping.find(joystick_axis) != joystick_mapping.end();
		}
		void unbind_joystick(joystick_axis_t joystick_axis)
		{
			joystick_mapping.erase(joystick_axis);
		}

		private:
			struct key_descriptor
			{
				axis_types_enum axis;
				float val_unpressed;
				float val_pressed;
			};
			struct joystick_descriptor
			{
				axis_types_enum axis;
				float bias_coeffecient;
			};
			std::unordered_map<scancode_t, key_descriptor> scancode_mapping;
			std::array<scancode_t, static_cast<size_t>(axis_types_enum::SIZE)> num_keys_controlling_axis;

			std::unordered_map<joystick_axis_t, joystick_descriptor> joystick_mapping;

			//This listener is special due to the increased complexity in converting buttons into axis input.
			event::listener<computer::scan_code::interface_t> scan_code_listener;
			event::listener<axis::joystick::interface_t> joystick_listener;
	};
	template<auto member_ptr_to_id_code, auto member_ptr_to_state, typename self_t>
	auto get_mapping_listener(self_t& self, auto& mapping_map)
	{
		return [&](auto in)
		{
			auto mapping_it = mapping_map.find(in.*member_ptr_to_id_code);
			if (mapping_it != mapping_map.end())
			{
				/*using vector_t = typename self_t::manager_t::queued_events_t;

				auto data = vector_t{in.*member_ptr_to_state};
				self.managers[static_cast<size_t>(mapping_it->second)].dump_events(data,nullptr);*/
				self.managers.dump_event(mapping_it->second, in.*member_ptr_to_state);
			}
		};
	}
	template<has_size_element button_types_enum, template<typename> typename managers_creator_t = event_module_based_managers_button_t>
	struct remappable_buttons_t
	{
		using scancode_t = uint32_t;
		using joystick_button_t = uint8_t;

		using val_t = bool;

		using interface_t = generic_button::interface_t;
		using event_configuration_types = generic_button::event_configuration_types;

		inline remappable_buttons_t(input::interfaces & input_if, auto ... input) : 
			managers(input...),
			scan_code_listener(input_if.scan_code, get_mapping_listener<&computer::scan_code::event_data_t::scan_code, &computer::scan_code::event_data_t::is_down>(*this, scancode_mapping),{}),
			mouse_button_listener(input_if.mouse_button, get_mapping_listener<&computer::mouse_button::event_data_t::button, &computer::mouse_button::event_data_t::is_down>(*this, mouse_button_mapping),{}),
			joystick_button_listener(input_if.joystick_button, get_mapping_listener<&axis::joystick_button::event_data_t::button, &axis::joystick_button::event_data_t::is_down>(*this, joystick_button_mapping),{})
		{
		}

		managers_creator_t<button_types_enum> managers;

		auto& operator[](button_types_enum enum_in)
		{
			return managers[enum_in];
		}
		void hook_key_to_button(scancode_t key, button_types_enum button)
		{
			scancode_mapping.insert(std::make_pair(key,button));
		}
		bool is_key_bound(scancode_t key)
		{
			return scancode_mapping.find(key) != scancode_mapping.end();
		}
		void unbind_key(scancode_t key)
		{
			scancode_mapping.erase(key);
		}

		void hook_mouse_button_to_button(computer::mouse_button::button_t key, button_types_enum button)
		{
			mouse_button_mapping.insert(std::make_pair(key,button));
		}
		bool is_mouse_button_bound(scancode_t key)
		{
			return mouse_button_mapping.find(key) != mouse_button_mapping.end();
		}
		void unbind_mouse_button(scancode_t key)
		{
			mouse_button_mapping.erase(key);
		}

		void hook_joystick_button_to_button(joystick_button_t joystick_button, button_types_enum button)
		{
			joystick_button_mapping.insert(std::make_pair(joystick_button,button));
		}
		bool is_joystick_button_bound(scancode_t key)
		{
			return joystick_button_mapping.find(key) != joystick_button_mapping.end();
		}
		void unbind_joystick_button(scancode_t key)
		{
			joystick_button_mapping.erase(key);
		}

		private:
			std::unordered_map<scancode_t, button_types_enum> scancode_mapping;
			std::unordered_map<computer::mouse_button::button_t, button_types_enum> mouse_button_mapping;
			std::unordered_map<joystick_button_t, button_types_enum> joystick_button_mapping;

			event::listener<computer::scan_code::interface_t> scan_code_listener;
			event::listener<computer::mouse_button::interface_t> mouse_button_listener;
			event::listener<axis::joystick_button::interface_t> joystick_button_listener;

	};

	template<typename interface_t>
	class generic_t
	{
		event::listener<interface_t> listener;
		using val_t = typename interface_t::event_data_t;
		val_t val;
		public:
			generic_t(interface_t& interf) :
				listener(interf, [this](auto in)
				{
					val = in;
				},{}),
				val(0) //TODO: some sort of proper initialization
			{
			}
			val_t get()
			{
				return val;
			}
			val_t operator*()
			{
				return val;
			}
	};
	using generic_axis_value = generic_t<generic_axis::interface_t>;
	using generic_button_value = generic_t<generic_button::interface_t>;
}
