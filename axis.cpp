#include "axis.hpp"


namespace input
{
	namespace axis
	{
		namespace joystick
		{
			manager_t manager("joystick");
		}
		namespace joystick_button
		{
			manager_t manager("joystick_button");
		}
		std::unordered_map<int, deadzone_descriptor> deadzones;
		std::mutex deadzones_mutex;
	}
}

