# Input module

Using various backends, such as SDL and Module3D's XR module, the Input module allows easy receiving of input through a homegenous method as opposed to various different systems that different API's provide.

# Dependancies

Depends upon module3D's event module , and SDL2

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
