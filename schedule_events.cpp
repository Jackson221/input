#include "schedule_events.hpp"

#include <cstdint>
#include <unordered_map>

#include <SDL.h>

#include "input/computer.hpp"
#include "input/axis.hpp"
#include "input/interfaces.hpp"
#include "logger/logger.hpp"

#include "variadic_util/variadic_util.hpp"

namespace input
{

	struct backend_details_t
	{
		interfaces & input_if;
		static int callback(void* this_ptr, SDL_Event* event_ptr);
		std::unordered_map<int32_t, SDL_GameController*> controllers;

		uint32_t last_hat_button = std::numeric_limits<uint8_t>::max();
	};

	constexpr char nonexistant_button_msg[] = "SDL input gives mouse button unhandled by Module3D's input module: %d%\nIt will be assigned to left-click instead.";

	event_watcher::event_watcher(interfaces & input_if) :
		backend(std::make_unique<backend_details_t>(input_if))
	{
		for (int i = 0; i < SDL_NumJoysticks(); i++)
		{
			if (SDL_IsGameController(i))
			{
				open_controller(i);
			}
		}
		SDL_AddEventWatch(&backend_details_t::callback, this);
	}
	event_watcher::~event_watcher()
	{
		for (auto& [id, controller] : backend->controllers)
		{
			SDL_GameControllerClose(controller);
		}
		SDL_DelEventWatch(&backend_details_t::callback,this);
	}
	void event_watcher::open_controller(int id)
	{
		SDL_GameController* controller = SDL_GameControllerOpen(id);
		if (controller)
		{
			backend->controllers.insert(std::make_pair(id, controller));
		}
		else
		{
			printf("Failed to open controller %d: %s\n", id, SDL_GetError());
		}
	}
	float correct_value_for_deadzone(float value, float deadzone)
	{
		bool was_negative = value < 0.0;
		//if the value is negative but deadzone is positive, or vice versa, then there is nothing to do
		if (  (was_negative && deadzone > 0.0) || (!was_negative  && deadzone < 0.0) )
		{
			return value;
		}

		if (was_negative)
		{
			value *= -1;
			deadzone *= -1;
		}

		//If subtracting the deadzone leaves a negative value, it was inside the deadzone, so set
		//it to zero.
		value -= deadzone;
		if (value < 0)
		{
			value = 0;
		}

		//Since `deadzone` is now 0, what was 1.0 will now be (1.0-deadzone)
		//This will correct for it and change the curve such that the old 1.0
		//is 1.0 again.
		double new_max_val = 1.0 - deadzone;
		value /= new_max_val;

		return value * (was_negative? -1.0 : 1.0);
	}
	float correct_axis_for_deadzone(int axis, float value)
	{
		auto deadzone_it = axis::deadzones.find(axis);
		if (deadzone_it != axis::deadzones.end())
		{
			auto _ = std::lock_guard(axis::deadzones_mutex);
			value = correct_value_for_deadzone(value, deadzone_it->second.positive_deadzone);
			value = correct_value_for_deadzone(value, deadzone_it->second.negative_deadzone);
		}
		return value;
	}


	int backend_details_t::callback(void* this_ptr, SDL_Event* event_ptr)
	{
		event_watcher& self = *static_cast<event_watcher*>(this_ptr);
		SDL_Event event = *event_ptr;
		//these have to be kept up here as sdl does not combine mouse motion & scroll wheel motion but module3d does.
		//TODO: De-combine them
		computer::mouse_motion::event_data_t mouse_motion_data = {0,0,0,0,0,0};

		std::array<bool, 8> convert_to_0_to_1_axes;
		convert_to_0_to_1_axes.fill(false);
		convert_to_0_to_1_axes[2] = true;
		convert_to_0_to_1_axes[5] = true;

		interfaces & input_if = self.backend->input_if;

		float axis_value;
		switch(event.type)
		{
			case SDL_KEYUP:
				input_if.scan_code.push_event({false,event.key.keysym.scancode});	
				input_if.scan_code.send_events();
				break;
			case SDL_KEYDOWN:
				input_if.scan_code.push_event({true,event.key.keysym.scancode});	
				input_if.scan_code.send_events();
				if (event.key.keysym.scancode == SDL_SCANCODE_V && SDL_GetModState() & KMOD_CTRL)
				{
					input_if.clipboard.push_event(computer::clipboard::get());
					input_if.clipboard.send_events();
				}
				break;
			case SDL_MOUSEMOTION:
				mouse_motion_data.delta_x = event.motion.xrel;
				mouse_motion_data.delta_y = event.motion.yrel;
				mouse_motion_data.x = event.motion.x;
				mouse_motion_data.y = event.motion.y;

				computer::mouse_motion::current_cursor_x = static_cast<float>(mouse_motion_data.x);
				computer::mouse_motion::current_cursor_y = static_cast<float>(mouse_motion_data.y);

				input_if.mouse_motion.push_event(mouse_motion_data);
				input_if.mouse_motion.send_events();
				break;
			case SDL_TEXTINPUT:
				input_if.text_input.push_event({event.text.text});
				input_if.text_input.send_events();
			case SDL_MOUSEWHEEL:
				mouse_motion_data.delta_wheel_x = event.wheel.x;
				mouse_motion_data.delta_wheel_y = event.wheel.y;
				mouse_motion_data.x = static_cast<uint32_t>(computer::mouse_motion::current_cursor_x);
				mouse_motion_data.y = static_cast<uint32_t>(computer::mouse_motion::current_cursor_y);
				input_if.mouse_motion.push_event(mouse_motion_data);
				input_if.mouse_motion.send_events();
				break;
			case SDL_MOUSEBUTTONUP:
				[[fallthrough]];
			case SDL_MOUSEBUTTONDOWN:
				computer::mouse_button::button_t button;
				using computer::mouse_button::button_t;
				switch(event.button.button)
				{
					case SDL_BUTTON_LEFT:
						button = button_t::left;
						break;
					case SDL_BUTTON_RIGHT:
						button = button_t::right;
						break;
					case SDL_BUTTON_MIDDLE:
						button = button_t::middle;
						break;
					case SDL_BUTTON_X1:
						button = button_t::x1;
						break;
					case SDL_BUTTON_X2:
						button = button_t::x2;
						break;
					default:
						logger::error_log<nonexistant_button_msg>(event.button.button);
						button = button_t::left;
						break;
				}
				input_if.mouse_button.push_event({event.type == SDL_MOUSEBUTTONDOWN,button, event.button.x,event.button.y});
				input_if.mouse_button.send_events();
				break;
			case SDL_JOYAXISMOTION:
				axis_value = static_cast<float>(event.jaxis.value);

				if (event.jaxis.axis < convert_to_0_to_1_axes.size() && 
				    convert_to_0_to_1_axes[event.jaxis.axis])
				{
					axis_value += 32768.f;
					axis_value /= (32767.f + 32768.f);
				}
				else
				{
					if (axis_value > 0)
					{
						axis_value /= 32767.f;
					}
					else
					{
						axis_value /= 32768.f;
					}
				}
				axis_value = correct_axis_for_deadzone(event.jaxis.axis, axis_value);
				input_if.joystick.push_event({event.jaxis.axis,axis_value});	
				input_if.joystick.send_events();
				break;
			case SDL_JOYBUTTONDOWN:
				[[fallthrough]];
			case SDL_JOYBUTTONUP:
				input_if.joystick_button.push_event({event.jbutton.button, event.type == SDL_JOYBUTTONDOWN});
				input_if.joystick_button.send_events();
				break;
			case SDL_JOYHATMOTION:
				{
					uint8_t virtual_button = 254-8;
					bool down = true;
					switch(event.jhat.value)
					{
						case SDL_HAT_CENTERED:
							if (self.backend->last_hat_button == std::numeric_limits<uint8_t>::max())
							{
								return 0;
							}
							down = false;
							virtual_button = self.backend->last_hat_button;
							break;
						case SDL_HAT_RIGHT:
							virtual_button += 1;
							break;
						case SDL_HAT_LEFT:
							virtual_button += 2;
							break;
						case SDL_HAT_UP:
							virtual_button += 3;
							break;
						case SDL_HAT_DOWN:
							virtual_button += 4;
							break;
						case SDL_HAT_RIGHTUP:
							virtual_button += 5;
							break;
						case SDL_HAT_RIGHTDOWN:
							virtual_button += 6;
							break;
						case SDL_HAT_LEFTUP:
							virtual_button += 7;
							break;
						case SDL_HAT_LEFTDOWN:
							virtual_button += 8;
							break;

					}
					input_if.joystick_button.push_event({virtual_button, down});
					input_if.joystick_button.send_events();
				}
				break;
			case SDL_CONTROLLERDEVICEADDED:
				self.open_controller(event.cdevice.which);	
				break;
			case SDL_CONTROLLERDEVICEREMOVED:
				if(self.backend->controllers.find(event.cdevice.which) != self.backend->controllers.end())
				{
					SDL_GameControllerClose(self.backend->controllers[event.cdevice.which]);
					self.backend->controllers.erase(event.cdevice.which);
				}
				break;
			case SDL_QUIT:
				input_if.quit.push_event({});
				input_if.quit.send_events();
				break;
			default:
				break;
		}

		/*variadic_util::for_each_element_in_variadic(computer::interfaces,[](auto interface)
		{
			interface->send_events();
		});

		axis::joystick::interface.send_events();
		*/

		return 0;
	}
}
