/*!
 * @file
 *
 * @author Jackson McNeill
 *
 *
 * Provides event instantiations for devices such as keyboards, mice, controllers, etc.
 */
#pragma once 

#include "event/event.hpp"
#include <tuple>
#include <string>

namespace input
{
	namespace computer
	{
		namespace scan_code
		{
			struct event_data_t
			{
				bool is_down;
				uint32_t scan_code;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
		}
		namespace text_input
		{
			struct event_data_t
			{
				std::string text;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;

			void start_input();
			void stop_input();

			bool is_inputting();
		}
		namespace mouse_motion
		{
			struct event_data_t
			{
				int32_t delta_x;
				int32_t delta_y;
				
				int32_t x;
				int32_t y;

				int32_t delta_wheel_x;
				int32_t delta_wheel_y;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;

			extern float current_cursor_x;
			extern float current_cursor_y;
		}
		namespace mouse_button
		{
			enum class button_t
			{
				left,
				middle,
				right,
				x1,
				x2,
				SIZE
			};
			struct event_data_t
			{
				bool is_down;
				button_t button;

				int32_t x;
				int32_t y;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
		}
		namespace clipboard
		{
			using event_data_t = std::string;

			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;

			void set(std::string txt);
			std::string get();
		}
		namespace quit
		{
			struct event_data_t {};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
		}
	}
}
