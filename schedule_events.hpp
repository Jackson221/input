/*!
 * Schedules all events under the event modules (keyboard, mouse, joystick, etc. input). Runs under the same thread that schedule_events() is called in.
 */
#pragma once

#include <memory>

namespace input
{
	struct backend_details_t;

	struct interfaces;

	class event_watcher
	{
		std::unique_ptr<backend_details_t> backend;
		friend struct backend_details_t;

		void open_controller(int id);
		public:
			event_watcher(interfaces & input_if);
			~event_watcher();
	};
}
