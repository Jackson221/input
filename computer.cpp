#include "computer.hpp"

#include "SDL.h"

#include <atomic>
namespace input
{
	namespace computer
	{

		namespace scan_code
		{
			manager_t manager("scan_code");
		}
		namespace text_input
		{
			manager_t manager("text_input");
			std::atomic<bool> on = false;

			void start_input()
			{
				SDL_StartTextInput();
				on = true;
			}
			void stop_input()
			{
				SDL_StopTextInput();
				on = false;
			}
			bool is_inputting()
			{
				return on.load();
			}
		}
		namespace mouse_motion
		{
			manager_t manager("mouse_motion");

			float current_cursor_x;
			float current_cursor_y;
		}
		namespace mouse_button
		{
			manager_t manager("mouse_button");
		}
		namespace clipboard
		{
			manager_t manager("clipboard");

			void set(std::string txt)
			{
				SDL_SetClipboardText(txt.c_str());
			}
			std::string get()
			{
				char* txt_c_str = SDL_GetClipboardText();
				std::string result = txt_c_str;
				SDL_free(txt_c_str);
				return result;
			}
		}
		namespace quit
		{
			manager_t manager("quit");
		}
	}
}
